#!/bin/bash

chown www-data:www-data /app -R
source /etc/apache2/envvars
[ ! -d ${APACHE_RUN_DIR:-/var/run/apache2} ] && mkdir -p ${APACHE_RUN_DIR:-/var/run/apache2}
[ ! -d ${APACHE_LOCK_DIR:-/var/lock/apache2} ] && mkdir -p ${APACHE_LOCK_DIR:-/var/lock/apache2} && chown ${APACHE_RUN_USER:-www-data} ${APACHE_LOCK_DIR:-/var/lock/apache2}
#exec /usr/sbin/apache2 -e debug >> /var/log/apache.log 2>&1
#tail -F /var/log/apache2/* &
#exec apache2 -D FOREGROUND
exec apache2ctl -e debug -DFOREGROUND > /var/log/apache2/all.log
